.. _beaglebone-blue-home:

BeagleBone Blue
################

To optimize BeagleBone for education, BeagleBone Blue was created that integrates many components 
for robotics and machine control, including connectors for off-the-shelf robotic components. 
For education, this means you can quickly start talking about topics such as programming and 
control theory, without needing to spend so much time on electronics. The goal is to still be 
very hackable for learning electronics as well, including being fully open hardware.

BeagleBone Blue's legacy is primarily from contributions to BeagleBone Black 
robotics by `UCSD Flow Control and Coordinated Robotics Lab <http://robotics.ucsd.edu/>`_, 
`Strawson Design <http://www.strawsondesign.com/>`_, `Octavo Systems <http://octavosystems.com/>`_, 
`WowWee <http://www.wowwee.com/mip/>`_, `National Instruments LabVIEW <http://www.ni.com/labview/>`_ 
and of course the `BeagleBoard.org Foundation <https://beagleboard.org/about>`_.

.. admonition:: Contributors

    This work is licensed under a `Creative Commons Attribution-ShareAlike
    4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__

.. note::
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page. 
    
    Use of either the boards or the design materials constitutes agreement to the T&C including any 
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.


.. toctree::
   :maxdepth: 1
   
   /boards/beaglebone/blue/pinouts.rst
   /boards/beaglebone/blue/ssh.rst
   /boards/beaglebone/blue/wifi.rst
   /boards/beaglebone/blue/ip-settings.rst
   /boards/beaglebone/blue/flashing-firmware.rst
   /boards/beaglebone/blue/code.rst
   /boards/beaglebone/blue/tests.rst
   /boards/beaglebone/blue/accessories.rst
   /boards/beaglebone/blue/faq.rst
