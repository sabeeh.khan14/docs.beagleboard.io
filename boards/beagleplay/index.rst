.. _beagleplay-home:

BeaglePlay
##########

.. important::
    This is a work in progress, for latest documentation please 
    visit https://docs.beagleboard.org/latest/


.. table::
   :align: center
   :widths: auto

   +----------------------------------------------------+---------------------------------------------------------+
   | .. image:: images/front.webp                       | .. image:: images/back.webp                             |
   |    :width: 700                                     |       :width: 700                                       |
   |    :align: center                                  |       :align: center                                    |
   |    :alt: BeaglePlay                                |       :alt: BeaglePlay                                  |
   +----------------------------------------------------+---------------------------------------------------------+

.. raw:: latex
   
   \begin{comment}

.. grid:: 1 1 2 3
   :margin: 4 4 0 0
   :gutter: 4

   .. grid-item-card::
      :link: beagleplay-introduction
      :link-type: ref

      **1. Introduction**
      ^^^

      .. image:: images/chapter-thumbnails/01-introduction.jpg
         :align: center
         :alt: BeaglePlay Chapter01 thumbnail
      
      +++

      Introduction to BeaglePlay board with information on each component 
      location on both front and back of the board.

   .. grid-item-card:: 
      :link: beagleplay-quick-start
      :link-type: ref

      **2. Quick start**
      ^^^

      .. image:: images/chapter-thumbnails/02-quick-start.jpg
         :align: center
         :alt: BeaglePlay Chapter02 thumbnail

      +++

      Getting started guide to enable you to start building your projects 
      in no time.

   .. grid-item-card:: 
      :link: beagleplay-design
      :link-type: ref

      **3. Design & Specifications**
      ^^^

      .. image:: images/chapter-thumbnails/03-design-and-specifications.jpg
         :align: center
         :alt: BeaglePlay Chapter03 thumbnail

      +++

      Hardware and mechanical design and specifications of the BeaglePlay board 
      for those who want to know their board inside and out.

   .. grid-item-card:: 
      :link: beagleplay-connectors-expansion
      :link-type: ref

      **4. Expansion**
      ^^^

      .. image:: images/chapter-thumbnails/04-connectors-and-pinouts.jpg
         :align: center
         :alt: BeaglePlay Chapter04 thumbnail

      +++

      Connector pinout diagrams with expansion details so that you can 
      easily debug your connections and create custom expansion hardware.

   .. grid-item-card:: 
      :link: beagleplay-support
      :link-type: ref

      **5. Support**
      ^^^

      .. image:: images/chapter-thumbnails/06-support-documents.jpg
         :align: center
         :alt: BeaglePlay Chapter10 thumbnail

      +++

      Additional supporting information, images, documents, change history and
      hardware & software repositories including issue trackers.

.. raw:: latex

   \end{comment}

.. toctree::
   :maxdepth: 1
   :hidden:

   01-introduction
   02-quick-start
   03-design
   04-expansion
   05-support
