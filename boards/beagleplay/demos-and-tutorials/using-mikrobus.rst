.. _beagleplay-mikrobus:

Using mikroBUS
##############

Steps:

1. Identify if mikroBUS add-on includes an ID. If not, ID must be supplied.
2. Identify if mikroBUS add-on is supported by the kernel. If not, kernel module must be added.
3. Identify how driver exposes the data: IIO, net, etc.
4. Connect and power
5. Verify and utilize
