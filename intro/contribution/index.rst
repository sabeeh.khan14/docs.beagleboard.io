.. _contribution:

Contribution 
###############

.. note::
   This section is under developmement right now.

.. important::
   First off, thanks for taking the time to think about contributing!

.. note::
   For donations, see `BeagleBoard.org - Donate <https://beagleboard.org/donate>`__.

The BeagleBoard.org Foundation maintains source for many open source projects.

Example projects suitable for first contributions:

* `BeagleBoard project documentation <https://git.beagleboard.org/docs/docs.beagleboard.io>`__
* `Debian image bug repository <https://git.beagleboard.org/beagleboard/Latest-Images>`__
* `Debian image builder <https://git.beagleboard.org/beagleboard/image-builder>`__

These guidelines are mostly suggestions, not hard-set rules. Use your best judgment, and feel free
to propose changes to this document in a pull request.

Code of Conduct
***************

This project and everyone participating are governed by the same code of conduct.

.. note::
   Check out https://forum.beagleboard.org/faq as a starting place for our code of conduct.

By participating, you are expected to
uphold this code. Please report unacceptable behavior to
contact one of our administrators or moderators on https://forum.beagleboard.org/about.

Frequently Asked Questions
**************************

Please refer to the technical and contribution frequently asked questions pages before posting any of your own questions. Please
feel encouraged to ask follow-up questions if any of the answers are not clear enough.

* `Frequently asked questions contribution category on the BeagleBoard.org Forum <https://forum.beagleboard.org/c/faq>`__

What should I know before I get started?
****************************************

The more you know about Linux and contributing to upstream projects, the better, but this knowledge isn't strictly required. Simply
reading about contributing to Linux and upstream projects can help build your vocabulary in a meaningful way to help out. Learn about
the skills required for Linux contributions in the :ref:`beagleboard-linux-upstream` section.

The most useful thing to know is how to ask smart questions. Read about this in the :ref:`intro-getting-support` section. If you ask
smart questions on the issue trackers and forum, you'll be doing a lot to help us improve the designs and documentation.

.. toctree::
   :maxdepth: 1
   :hidden:

   /intro/contribution/linux-upstream

How can I contribute?
*********************

The most obvious way to contribute is using the `git.beagleboard.org Gitlab server <https://git.beagleboard.org>`_ to report
bugs, suggest enhancements and providing merge requests, also called pull requests, the provide fixes to software, hardware
designs and documentation.

Reporting bugs
===============

Suggesting enhancements
=======================

Submitting merge requests
=========================

Style and usage guidelines
**************************

* :ref:`beagleboard-git-usage`
* Git commit messages
* :ref:`beagleboard-doc-style`

.. toctree::
   :maxdepth: 1
   :hidden:

   /intro/contribution/git-usage
   /intro/contribution/style
   /intro/contribution/rst-cheat-sheet
