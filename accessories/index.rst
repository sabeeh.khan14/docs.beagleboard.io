.. _accessories-home:

Accessories
#############

This section will include all the tested accessories for BeagleBoard.org hardware and kits.

.. note::

    This documentation is not complete and we are actively looking for volunteers to test 
    and add new hardware accessories that they have already with them. General feedback and contribution 
    is also appreciated. You can checkout these pages to contribuite to BeagleBoard.org docs project,\
    
    1. `Docs contribution guide <https://docs.beagleboard.io/latest/intro/contribution/index.html>`_
    2. `Docs project issue tracker. <https://git.beagleboard.org/docs/docs.beagleboard.io/-/issues>`_

.. danger::

    Accessories section contains only 3rd party products that have been manually used by community members with BeagleBoard.org products. 
    BeagleBoard.org should not be held liable for the functionality of BeagleBoard.org products in association with these 3rd party 
    products in any way possible. This is just a place for people to report their experiences and not a statement of compatibility. 
    BeagleBoard.org approve that these items have at least some aspect of testing by foundation members, though only specific 
    versions and it is up to the manufacturers of those items to maintain compatibility.

.. grid:: 1 1 2 3
   :margin: 4 4 0 0
   :gutter: 4

   .. grid-item-card::
      :link: accessories-power-supplies
      :link-type: ref

      **Power Supplies**
      ^^^

      .. image:: images/cards/powersupplies.jpg
         :align: center
      +++

      Power source for all your BeagleBoard.org hardware.


   .. grid-item-card::
      :link: accessories-displays
      :link-type: ref

      **Displays**
      ^^^

      .. image:: images/cards/displays.jpg
         :align: center
      +++

      Dedicated, portable, and TV monitors.

   .. grid-item-card:: 
      :link: accessories-peripherals
      :link-type: ref

      **Peripherals**
      ^^^

      .. image:: images/cards/peripherals.jpg
         :align: center
      +++

      Keyboard, mice, and other peripherals.

   .. grid-item-card:: 
      :link: accessories-cables
      :link-type: ref

      **cables**
      ^^^

      .. image:: images/cards/cables.jpg
         :align: center
      +++

      USB, debug, HDMI, and other cables.

   .. grid-item-card:: 
      :link: accessories-adapters
      :link-type: ref

      **Adapters**
      ^^^

      .. image:: images/cards/adapters.jpg
         :align: center
      +++

      Display adapters

   .. grid-item-card:: 
      :link: accessories-cameras
      :link-type: ref

      **Cameras**
      ^^^

      .. image:: images/cards/cameras.jpg
         :align: center
      +++

      USB and CSI cameras



.. toctree::
   :maxdepth: 1
   :hidden:

   /accessories/power-supplies
   /accessories/displays
   /accessories/peripherals
   /accessories/cables
   /accessories/adapters
   /accessories/cameras
