.. _accessories-adapters:

Adapters 
#########

HDMI Adapters
***************

microHDMI to VGA
=================

`Cable Matters Micro HDMI to VGA Adapter <https://www.amazon.com/Cable-Matters-Active-Female-Adapter/dp/B00879EZJI/ref=sr_1_2?ie=UTF8&qid=1381610066&sr=8-2&keywords=micro-hdmi+to+vga>`_


HDMI to Composite/CVBS/RCA AV Video Converter
===============================================

- `Sounce HDMI to RCA,HDMI to AV <https://www.amazon.in/Sounce-Composite-Converter-Supports-DVD-Black/dp/B098DMMS3Z>`_

HDMI-Female to Micro HDMI-Male Adapter
=======================================

- `Lapster Micro HDMI Male to HDMI Female <https://www.amazon.in/Lapster-Micro-Female-Converter-Adapter/dp/B08PP924DK>`_